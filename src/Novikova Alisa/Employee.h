#pragma once
using namespace std;

class Employee
{
protected:
	int id; // ����������������� �����
	string name; // ���
	int time = 40 ; // ������������ ����� � �����
	int payment; // ���������� �����
	int budget; // ������ �������
	int numberEmp; // ���������� �����������
	string position; // ���������
public:
	Employee() {};

	virtual void Pay(){}
	int GetTime() const { return time; }
	int GetPayment() const { return payment; }
	int getID() { return id; }
	string GetName() const { return name; }
	string GetPosition() const { return position; }
	void SetPosition(string position) { this->position = position; }
};
